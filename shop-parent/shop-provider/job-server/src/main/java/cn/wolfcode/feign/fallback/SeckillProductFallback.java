package cn.wolfcode.feign.fallback;

import cn.wolfcode.common.web.Result;
import cn.wolfcode.domain.SeckillProductVo;
import cn.wolfcode.feign.SeckillProductFeignApi;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

public class SeckillProductFallback implements SeckillProductFeignApi {
    @Override
    public Result<List<SeckillProductVo>> selectByTime(@RequestParam("time") Integer time) {
        // TODO: 2021/12/4 做兜底数据
        return null;
    }
}
