package cn.wolfcode.config;

import cn.wolfcode.job.SeckillProductInitJob;
import cn.wolfcode.job.UserCacheJob;
import cn.wolfcode.util.ElasticJobUtil;
import com.dangdang.ddframe.job.lite.config.LiteJobConfiguration;
import com.dangdang.ddframe.job.lite.spring.api.SpringJobScheduler;
import com.dangdang.ddframe.job.reg.base.CoordinatorRegistryCenter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class BusinessJobConfig {
    @Bean(initMethod = "init")
    public SpringJobScheduler initUserCacheJob(CoordinatorRegistryCenter registryCenter, UserCacheJob userCacheJob){
        LiteJobConfiguration jobConfiguration = ElasticJobUtil.createDefaultSimpleJobConfiguration(userCacheJob.getClass(), userCacheJob.getCron());
        return new SpringJobScheduler(userCacheJob, registryCenter,jobConfiguration );
    }
    //定时任务=>秒杀商品初始化任务的配置和初始化
    @Bean(initMethod = "init")
    public SpringJobScheduler initSeckillProductJob(CoordinatorRegistryCenter registryCenter, SeckillProductInitJob seckillProductInitJob){
        LiteJobConfiguration jobConfiguration = ElasticJobUtil.createJobConfiguration(seckillProductInitJob.getClass(),
                seckillProductInitJob.getCron(),//任务执行时间规则
                seckillProductInitJob.getShardingTotalCount(),//任务分片数量
                seckillProductInitJob.getShardingItemParameters(),//任务分片规则
                false//是否分批
                );
        return new SpringJobScheduler(seckillProductInitJob, registryCenter,jobConfiguration );
    }
}
