package cn.wolfcode.feign;

import cn.wolfcode.common.web.Result;
import cn.wolfcode.domain.SeckillProductVo;
import cn.wolfcode.feign.fallback.SeckillProductFallback;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@FeignClient(name = "seckill-service",fallback = SeckillProductFallback.class)
public interface SeckillProductFeignApi {
    @RequestMapping("/seckillProduct/selectByTime")
    Result<List<SeckillProductVo>> selectByTime(@RequestParam("time") Integer time);
}
