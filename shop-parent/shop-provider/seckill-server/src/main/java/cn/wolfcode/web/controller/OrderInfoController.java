package cn.wolfcode.web.controller;

import cn.wolfcode.common.constants.CommonConstants;
import cn.wolfcode.common.domain.UserInfo;
import cn.wolfcode.common.exception.BusinessException;
import cn.wolfcode.common.web.Result;
import cn.wolfcode.common.web.anno.RequireLogin;
import cn.wolfcode.domain.SeckillProductVo;
import cn.wolfcode.redis.CommonRedisKey;
import cn.wolfcode.redis.SeckillRedisKey;
import cn.wolfcode.service.IOrderInfoService;
import cn.wolfcode.service.ISeckillProductService;
import cn.wolfcode.util.DateUtil;
import cn.wolfcode.util.UserUtil;
import cn.wolfcode.web.msg.SeckillCodeMsg;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.spring.core.RocketMQTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/order")
@Slf4j
public class OrderInfoController {
    @Autowired
    private ISeckillProductService seckillProductService;
    @Autowired
    private StringRedisTemplate redisTemplate;
    @Autowired
    private RocketMQTemplate rocketMQTemplate;
    @Autowired
    private IOrderInfoService orderInfoService;

    @RequireLogin
    @RequestMapping("/doSeckill")
    public Result<String> doSeckill(@RequestHeader(CommonConstants.TOKEN_NAME) String userToken, Long seckillId, Integer time){
        //1.判断用户是否登陆
        // 2.判断参数是否合法
        if (seckillId == null || time == null){
            log.error("【秒杀商品订单服务】：传入参数异常");
            throw new BusinessException(SeckillCodeMsg.ILLEGAL_ARGUMENT_ERROR);
        }
        // 3.基于秒杀商品id获取秒杀商品对象，如果对象不存在， 也说明是非法操作
        SeckillProductVo seckillProductVo = seckillProductService.queryByTimeAndSeckillId(String.valueOf(time), String.valueOf(seckillId));
        if (seckillProductVo == null){
            log.error("【秒杀商品订单服务】：秒杀商品不存在");
            throw new BusinessException(SeckillCodeMsg.PRODUCT_NULL_ERROR);
        }
        // 4.判断是否在活动时间范围内
//        if (!DateUtil.isLegalTime(seckillProductVo.getStartDate(),time)){
//            log.error("【秒杀商品订单服务】：不在活动时间范围内");
//            throw new BusinessException(SeckillCodeMsg.TIME_ERROR);
//        }
        // 5.判断库存是否>0
        if (seckillProductVo.getStockCount() <= 0){
            log.error("【秒杀商品订单服务】：商品已经抢完");
            throw new BusinessException(SeckillCodeMsg.SECKILL_STOCK_OVER);
        }
        // 6.判断用户是否重复下单.
        String realKey = SeckillRedisKey.SECKILL_ORDER_SET.getRealKey(String.valueOf(time));
        UserInfo user = UserUtil.getUser(redisTemplate, userToken);
        Boolean member = redisTemplate.opsForSet().isMember(realKey, user.getPhone() + ":" + seckillId);
        if (member != null && member){
            log.error("【秒杀商品订单服务】：已经下单过");
            throw new BusinessException(SeckillCodeMsg.REPEAT_SECKILL);
        }
        // 7.开始秒杀流程，减库存、创建秒杀订单、存储下单成功标识，返回订单编号
        String orderNo = orderInfoService.doSeckill(seckillProductVo,user);
        return Result.success(orderNo);
    }
}
