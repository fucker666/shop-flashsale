package cn.wolfcode.service;

import cn.wolfcode.domain.SeckillProduct;
import cn.wolfcode.domain.SeckillProductVo;

import java.util.List;


public interface ISeckillProductService {
    /**
     * 根据秒杀开始时间段查询vo对象
     * @param time 秒杀时间段
     * @return vo对象列表
     */
    List<SeckillProductVo> selectByTime(Integer time);

    /**
     * 根据时间和秒杀商品Id查询秒杀商品vo信息
     * @param time 时间
     * @param seckillId 秒杀商品Id
     * @return 秒杀商品vo对象
     */
    SeckillProductVo queryByTimeAndSeckillId(String time, String seckillId);

    /**
     * 在redis中查询抢购商品信息
     * @param time 场次
     * @return 秒杀商品信息
     */
    List<SeckillProductVo> selectByTimeFromRedis(Integer time);

    /**
     * 扣除秒杀商品库存
     * @param seckillProductVo 秒杀商品
     */
    void decrStockCount(SeckillProductVo seckillProductVo);
}
