package cn.wolfcode.feign;

import cn.wolfcode.common.web.Result;
import cn.wolfcode.domain.Product;
import cn.wolfcode.feign.fallBack.ProductFeignFallBack;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Collection;
import java.util.List;

@FeignClient(name = "product-service" , fallback = ProductFeignFallBack.class)
public interface ProductFeignApi {
    @RequestMapping("/product/selectProductByIds")
    Result<List<Product>> selectProductByIds(@RequestParam("ids")Collection<Long> ids);
}
