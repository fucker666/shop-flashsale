package cn.wolfcode.service;


import cn.wolfcode.common.domain.UserInfo;
import cn.wolfcode.domain.OrderInfo;
import cn.wolfcode.domain.SeckillProductVo;

import java.util.Map;

/**
 * Created by wolfcode-lanxw
 */
public interface IOrderInfoService {

    /**
     * 生成订单信息。返回订单编号
     * @param seckillProductVo 秒杀商品
     * @param user 下单用户
     * @return 订单编号
     */
    String doSeckill(SeckillProductVo seckillProductVo, UserInfo user);
}
