package cn.wolfcode.feign.fallBack;

import cn.wolfcode.common.web.Result;
import cn.wolfcode.domain.Product;
import cn.wolfcode.feign.ProductFeignApi;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.List;

@Component
public class ProductFeignFallBack implements ProductFeignApi{
    @Override
    public Result<List<Product>> selectProductByIds(Collection<Long> ids) {
        // TODO: 2021/12/3  做兜底数据
        return null;
    }
}
