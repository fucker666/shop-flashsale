package cn.wolfcode.service;

import cn.wolfcode.domain.Product;

import java.util.Collection;
import java.util.List;
import java.util.Set;


public interface IProductService {
    List<Product> selectProductByIds(Collection<Long> ids);
}
