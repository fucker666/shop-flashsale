package cn.wolfcode.mapper;

import cn.wolfcode.domain.Product;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.List;


@Component
public interface ProductMapper {
    /**
     * 根据用户传入的id集合查询商品对象信息
     * @param ids 商品Id集合
     * @return 商品列表
     */
    List<Product> queryProductByIds(@Param("ids") Collection<Long> ids);
}
