package cn.wolfcode.common.exception;

import cn.wolfcode.common.web.Result;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created by wolfcode-lanxw
 */
@Slf4j
public class CommonControllerAdvice {
    @ExceptionHandler(BusinessException.class)
    @ResponseBody
    public Result<?> handleBusinessException(BusinessException ex){
        log.error("【统一异常处理】：参数异常",ex);
        return Result.error(ex.getCodeMsg());
    }
    @ExceptionHandler(Exception.class)
    @ResponseBody
    public Result<?> handleDefaultException(Exception ex){
        log.error("【统一异常处理】：参数异常",ex);//在控制台打印错误消息.
        return Result.defaultError();
    }
}
