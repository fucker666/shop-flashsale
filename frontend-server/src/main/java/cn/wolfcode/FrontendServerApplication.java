package cn.wolfcode;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.HashMap;

@SpringBootApplication
public class FrontendServerApplication {
    public static void main(String[] args) {
        SpringApplication.run(FrontendServerApplication.class, args);
    }

}
